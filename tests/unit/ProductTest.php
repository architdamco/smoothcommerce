<?php

namespace App\Tests;

use App\Entity\Product;
use Symfony\Component\Validator\Validation;

class ProductTest extends \Codeception\Test\Unit
{
    /**
     * @var \App\Tests\UnitTester
     */
    protected $tester;

    /**
     *   @var Symfony\Component\Validator\Validation
     */
    protected $validator;

    protected function _before()
    {
        $this->validator = Validation::createValidatorBuilder()
        ->enableAnnotationMapping(true)
        ->addDefaultDoctrineAnnotationReader()
        ->getValidator();
    }

    protected function _after()
    {
    }

    /**
     * test with valid data.
     */
    public function testProductIsCreatedWithValidData()
    {
        $product = new Product();
        $product->setName('Sik Sik Wat');
        $product->setSku('DISH775TGHY');
        $product->setPrice(5.99);
        $violations = $this->validator->validate($product);
        $this->assertTrue(count($violations) == 0);
    }

    /**
     * validate with valid Min length.
     */
    public function testProductIsValidatedWithMinNameData()
    {
        $product = new Product();
        $product->setName('P');
        $product->setSku('DISH775TGHY');
        $product->setPrice(5.99);
        $violations = $this->validator->validate($product);
        $this->assertTrue(count($violations) > 0);
    }

    /**
     * validate with Blank.
     */
    public function testProductNameIsValidatedWithBlank()
    {
        $product = new Product();
        $product->setName('');
        $product->setSku('DISH775TGHY');
        $product->setPrice(12.99);
        $violations = $this->validator->validate($product);
        $this->assertTrue(count($violations) > 0);
    }
}

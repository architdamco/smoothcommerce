<?php

namespace App\Tests;

use Codeception\Util\HttpCode;

class CategoryApiCest
{
    public function _before(ApiTester $I)
    {
    }

    // tests
    public function tryToTest(ApiTester $I)
    {
    }

    /**
     * validate list Api.
     *
     * @var ApiTester
     */
    public function listCategoryViaAPI(ApiTester $I)
    {
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendGet('category/');
        $I->seeResponseCodeIs(HttpCode::OK); // 200
        $I->seeResponseIsJson();
        $I->seeResponseContains('Ethiopia');
    }
}

<?php

namespace App\Tests;

use Codeception\Util\HttpCode;

class ProductApiCest
{
    /**
     * validate Add API.
     *
     * @var ApiTester
     */
    public function createProductViaAPI(ApiTester $I)
    {
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendPost('product/add', json_encode([
            'name' => 'Sik Sik Wat',
            'sku' => 'DISH999ABCD',
            'price' => 15.2,
            'categories' => [
                ['id' => 1],
            ],
        ]));
        $I->seeResponseCodeIs(HttpCode::CREATED); // 201
        $I->seeResponseIsJson();
        $I->seeResponseContains('DISH999ABCD');
    }

    /**
     * validate Add API.
     *
     * @var ApiTester
     */
    public function createProductErrorViaAPI(ApiTester $I)
    {
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendPost('product/add', json_encode([
        'name' => 'Sik Sik Wat',
        'sku' => 'DISH999ABCD',
        'price' => 15.2,
        ]));
        $I->seeResponseCodeIs(HttpCode::BAD_REQUEST);
        $I->seeResponseIsJson();
    }

    /**
     * validate Update Product Api.
     *
     * @var ApiTester
     */
    public function updateProductViaAPI(ApiTester $I)
    {
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendPut('product/2', json_encode([
            'name' => 'Sik Sik Wat1',
            'price' => 15.29,
            'sku' => 'DISH775TGHY',
            'categories' => [
                ['id' => 1],
            ],
        ]));
        $I->seeResponseCodeIs(HttpCode::OK); // 200
        $I->seeResponseIsJson();
        $I->seeResponseContains('Sik Sik Wat1');
    }

    /**
     * validate Single Product Api.
     *
     * @var ApiTester
     */
    public function singleProductViaAPI(ApiTester $I)
    {
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendGet('product/2');
        $I->seeResponseCodeIs(HttpCode::OK); // 200
        $I->seeResponseIsJson();
        $I->seeResponseContains('Sik Sik Wat');
    }

    /**
     * validate list Api.
     *
     * @var ApiTester
     */
    public function listProductViaAPI(ApiTester $I)
    {
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendGet('/product');
        $I->seeResponseCodeIs(HttpCode::OK); // 200
        $I->seeResponseIsJson();
        $I->seeResponseContains('Sik Sik Wat');
    }

    /**
     * validate Delete product Api.
     *
     * @var ApiTester
     */
    public function deleteProductViaAPI(ApiTester $I)
    {
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendDelete('product/7');
        $I->seeResponseCodeIs(HttpCode::NO_CONTENT);
    }
}

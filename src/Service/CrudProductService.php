<?php

namespace App\Service;

use App\Entity\Category;
use App\Entity\Product;
use App\Repository\CategoryRepository;
use App\Repository\ProductRepository;
use Doctrine\ORM\EntityManagerInterface;

class CrudProductService
{
    private $productRepository;
    private $categoryRepository;
    private $entityManagerInterface;

    public function __construct(ProductRepository $productRepository, CategoryRepository $categoryRepository, EntityManagerInterface $entityManagerInterface)
    {
        $this->productRepository = $productRepository;
        $this->categoryRepository = $categoryRepository;
        $this->entityManagerInterface = $entityManagerInterface;
    }

    /**
     * Fetching all the products.
     *
     * @return Product
     */
    public function list()
    {
        return $this->productRepository->findAll();
    }

    /**
     * save the product data.
     *
     *  @return Product
     */
    public function save(array $request)
    {
        $product = new Product();
        $product->setName($request['name']);
        $product->setSku($request['sku']);
        $product->setPrice($request['price']);

        foreach ($request['categories'] as $key => $categoryArray) {
            $category = $this->getCategoryById($categoryArray['id']);
            $product->addCategory($category);
        }
        $this->entityManagerInterface->persist($product);
        $this->entityManagerInterface->flush();

        return $product;
    }

    /**
     * @var int
     *
     * @return product object
     */
    public function getOneProduct(int $productId)
    {
        return $this->productRepository->findOneBy(['id' => $productId]);
    }

    /**
     * @var array
     * @var int
     *
     * @return Product
     */
    public function update(array $request, int $productId)
    {
        $product = $this->productRepository->find($productId);
        empty($request['name']) ? true : $product->setName($request['name']);
        empty($request['sku']) ? true : $product->setSku($request['sku']);
        empty($request['price']) ? true : $product->setPrice($request['price']);
        if (!empty($request['categories'])) {
            foreach ($request['categories'] as $key => $categoryArray) {
                $category = $this->getCategoryById($categoryArray['id']);
                $product->addCategory($category);
            }
        }
        $this->entityManagerInterface->flush();

        return $product;
    }

    /* @var
    *
    * return null
    */

    public function delete(int $productId)
    {
        $product = $this->productRepository->find($productId);
        $this->entityManagerInterface->remove($product);
        $this->entityManagerInterface->flush();

        return null;
    }

    /**
     * @var
     *
     * @return Category Object
     */
    public function getCategoryById(int $categoryById)
    {
        return $this->categoryRepository->find($categoryById);
    }
}

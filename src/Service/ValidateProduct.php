<?php

namespace App\Service;

use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Regex;
use Symfony\Component\Validator\Validation;

class ValidateProduct
{
    /**
     * @var array
     *
     * @return array $errors
     */
    public function validate(array $data)
    {
        $errors = [];
        $validator = Validation::createValidator();
        $violations = $validator->validate($data['name'], [
            new Length(['min' => 2]),
            new NotBlank(),
        ]);

        if (0 !== count($violations)) {
            // there are errors, now you can show them
            foreach ($violations as $violation) {
                $errors['name'][] = $violation->getMessage();
            }
        }

        $violations = $validator->validate($data['sku'], [
             new Length(['min' => 2]),
             new NotBlank(),
             new Regex("/^[a-z\-0-9]+$/i"),
        ]);
        if (0 !== count($violations)) {
            // there are errors, now you can show them
            foreach ($violations as $violation) {
                $errors['sku'][] = $violation->getMessage();
            }
        }

        $violations = $validator->validate($data['price'], [
            new Length(['min' => 2]),
            new NotBlank(),
        ]);
        if (0 !== count($violations)) {
            // there are errors, now you can show them
            foreach ($violations as $violation) {
                $errors['price'][] = $violation->getMessage();
            }
        }

        if (empty($data['categories'])) {
            $errors['category'][] = 'Category can not be empty';
        }

        return $errors;
    }
}

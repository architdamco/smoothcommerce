<?php

namespace App\Service;

use App\Entity\Category;
use App\Repository\CategoryRepository;
use Doctrine\ORM\EntityManagerInterface;

class CategoryService
{
    private $categoryRepository;
    private $entityManagerInterface;

    public function __construct(CategoryRepository $categoryRepository, EntityManagerInterface $entityManagerInterface)
    {
        $this->categoryRepository = $categoryRepository;
        $this->entityManagerInterface = $entityManagerInterface;
    }

    /**
     * Fetching all the category.
     *
     * @return Category
     */
    public function list()
    {
        return $this->categoryRepository->findAll();
    }
}

<?php

namespace App\Controller;

use App\Service\CategoryService;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\Response;

class CategoryController extends AbstractFOSRestController
{
    private $categoryService;

    public function __construct(CategoryService $categoryService)
    {
        $this->categoryService = $categoryService;
    }

    /**
     * @Rest\Get("/api/v1/category", name="category")
     *
     * @return json
     */
    public function index(): View
    {
        return View::create($this->categoryService->list(), Response::HTTP_OK);
    }
}

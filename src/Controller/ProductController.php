<?php

namespace App\Controller;

use App\Service\CrudProductService;
use App\Service\ValidateProduct;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api/v1/product", name="product.")
 */
class ProductController extends AbstractFOSRestController
{
    private $validateProduct;
    private $crudProductService;

    public function __construct(CrudProductService $crudProductService, ValidateProduct $validateProduct)
    {
        $this->validateProduct = $validateProduct;
        $this->crudProductService = $crudProductService;
    }

    /**
     * @Rest\Get("/", name="product")
     *
     * @return json
     */
    public function index(): View
    {
        return View::create($this->crudProductService->list(), Response::HTTP_OK);
    }

    /**
     * Add Product.
     *
     * @Rest\Post("/add", name="add")
     *
     * @return Json
     */
    public function addProduct(Request $request)
    {
        $this->validateJson($request);
        $data = json_decode($request->getContent(), true);
        $validate = $this->validateProduct->validate($data);
        if (!empty($validate)) {
            return View::create($validate, Response::HTTP_BAD_REQUEST);
        }

        return View::create($this->crudProductService->save($data), Response::HTTP_CREATED);
    }

    /**
     * One Product Details.
     *
     * @Rest\Get("/{id}", name="single")
     *
     * @return Json
     */
    public function getSingleProduct(int $id): View
    {
        return View::create($this->crudProductService->getOneProduct($id), Response::HTTP_OK);
    }

    /**
     * @Rest\Put("/{id}", name="update")
     *
     * @var Request
     * @var int
     *
     * @return Json
     */
    public function updateProduct(Request $request, int $id): View
    {
        $this->validateJson($request);
        $data = json_decode($request->getContent(), true);
        $product = $this->crudProductService->update($data, $id);

        return View::create($product, Response::HTTP_OK);
    }

    /**
     * @Rest\Delete("/{id}", name="delete")
     *
     * @var int
     *
     * @return Json
     */
    public function deleteProduct(int $id): View
    {
        return View::create($this->crudProductService->delete($id), Response::HTTP_NO_CONTENT);
    }

    /*
    * validate json
    *
    */
    public function validateJson($data)
    {
        if ($data->getContentType() !== 'json') {
            return View::create(['Input data is not a valid JSON'], Response::HTTP_BAD_REQUEST);
        }

        return $data;
    }
}
